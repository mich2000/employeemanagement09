﻿using System.Collections.Generic;
using EmplManagement.DAL.Models;

namespace EmplManagement.DAL.Repositories
{
    public interface IEmployeeRepository
    {
        Employee GetById(int Id);

        IEnumerable<Employee> GetAll();

        Employee Create(Employee model);

        Employee Update(Employee model);

        Employee Delete(int id);
    }
}
