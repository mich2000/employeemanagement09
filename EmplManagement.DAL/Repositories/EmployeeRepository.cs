﻿using System.Collections.Generic;
using EmplManagement.DAL.Classes;
using EmplManagement.DAL.Context;
using EmplManagement.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;

namespace EmplManagement.DAL.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly AppDbContext _context;
        private readonly ILogger<EmployeeRepository> _logger;

        public EmployeeRepository(AppDbContext context, ILogger<EmployeeRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public IEnumerable<Employee> GetAll()
        {
            return _context.Employees;
        }

        public Employee GetById(int id)
        {
            if(id == 0)
            {
                _logger.LogWarning("A user id can't be equal to 0.");
                throw ExceptionFactory.ArgsEx("employees's id","id");
            }
            return _context.Employees.Find(id);
        }

        public Employee Create(Employee employee)
        {
            if(employee == null)
            {
                _logger.LogWarning("A employee can not be equal to null");
                throw ExceptionFactory.NullEx("employee");
            }
            EntityEntry<Employee> newEmployee = _context.Employees.Add(employee);
            if (newEmployee != null && newEmployee.State == EntityState.Added)
            {
                int affectedRows = _context.SaveChanges();
                if (affectedRows > 0)
                {
                    _logger.LogInformation($"user  {employee.Id} has been added");
                    return newEmployee.Entity;
                }
            }
            return null;
        }

        public Employee Delete(int id)
        {
            if (id == 0)
            {
                _logger.LogWarning("A employee.Id can't be equal to 0.");
                throw ExceptionFactory.ArgsEx("employees's id", "id");
            }
            Employee employee = GetById(id);
            if (employee != null)
            {
                EntityEntry<Employee> deletedEmployee = _context.Employees.Remove(employee);
                if (deletedEmployee != null && deletedEmployee.State == EntityState.Deleted)
                {
                    int affectedRows = _context.SaveChanges();
                    if (affectedRows > 0)
                    {
                        _logger.LogInformation($"user  {employee.Id} has been deleted");
                        return deletedEmployee.Entity;
                    }
                }
            }
            return null;
        }

        public Employee Update(Employee employee)
        {
            if (employee.Id == 0)
            {
                _logger.LogWarning("A employee.Id can't be equal to 0.");
                throw ExceptionFactory.ArgsEx("employees's id", "employee.Id");
            }
            if (employee == null)
            {
                _logger.LogWarning("A employee can not be equal to null");
                throw ExceptionFactory.NullEx("employee");
            }
            EntityEntry<Employee> newEmployee = _context.Employees.Update(employee);
            if (newEmployee != null && newEmployee.State == EntityState.Modified)
            {
                int affectedRows = _context.SaveChanges();
                if (affectedRows > 0)
                {
                    _logger.LogInformation($"user  {employee.Id} has been edited");
                    return newEmployee.Entity;
                }
            }
            return null;
        }
    }
}