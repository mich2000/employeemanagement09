﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmplManagement.DAL.Models
{
    public class Employee
    {
        public Employee(int id, string name, string email, Department department, string photoPath)
        {
            Id = id;
            Name = name;
            Email = email;
            Department = department;
            PhotoPath = photoPath;
        }

        public Employee(string name, string email, Department department, string photoPath)
            :this(0,name, email, department, photoPath) { }

        public Employee(string name, string email, Department department)
            : this(0, name, email, department, "") { }

        public Employee() : this("", "", Department.None, "") { }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [RegularExpression(
            @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
            ErrorMessage = "The email is not in correct format.")]
        public string Email { get; set; }

        public Department Department { get; set; }

        public string PhotoPath { get; set; }

    }
}
