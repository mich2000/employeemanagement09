﻿namespace EmplManagement.DAL.Models
{
    public enum Department
    {
        None,
        IT,
        HR,
        Payroll
    }
}
