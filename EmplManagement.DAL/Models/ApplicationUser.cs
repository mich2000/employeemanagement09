﻿using Microsoft.AspNetCore.Identity;

namespace EmplManagement.DAL.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string City { get; set; }
    }
}
