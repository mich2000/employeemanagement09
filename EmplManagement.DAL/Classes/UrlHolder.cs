﻿using EmplManagement.DAL.Exceptions;
using System.Net;

namespace EmplManagement.DAL.Classes
{
    public class UrlHolder : IUrl
    {
        public UrlHolder(int statusCode, string path, string parameters)
        {
            StatusCode = statusCode;
            Path = path;
            Parameters = parameters;
        }

        public int StatusCode { get; set; }

        public string Path { get; set; }

        public string Parameters { get; set; }

        public string ErrorLog()
        {
            return $"{StatusCode} error occured. Path = {Path} and Querystring = {Parameters}";
        }

        public string NormalLog()
        {
            return $"Status {StatusCode}. Path = {Path} and Querystring = {Parameters}";
        }

        public string StatusCodeMessage()
        {
            switch (StatusCode)
            {
                case (int)HttpStatusCode.NotFound:
                    return "Sorry, the ressource you requested could not be found.";
                default:
                    return "Status code has not be found or implemented yet";
            }
        }
    }
}