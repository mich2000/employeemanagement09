﻿using System;

namespace EmplManagement.DAL.Classes
{
    /**
     * class used to write less long exceptions and show more standard responses.
     * **/
    public static class ExceptionFactory
    {
        public static ArgumentNullException NullEx(string parametername)
        {
            throw new ArgumentNullException(parametername, $"{parametername} can't equal to nothing.");
        }

        public static ArgumentNullException NullEx(string objectName, string parametername)
        {
            return new ArgumentNullException(parametername, $"{objectName} can't equal to nothing.");
        }

        public static ArgumentException ArgsEx(string parametername)
        {
            return new ArgumentNullException(parametername, $"{parametername} is not valid.");
        }

        public static ArgumentException ArgsEx(string objectName, string parametername)
        {
            return new ArgumentNullException(parametername, $"{objectName} is not valid.");
        }

        public static ArgumentException ArgsEx(string objectName, string objectedState, string parametername)
        {
            return new ArgumentNullException(parametername,$"{objectName} can't be equal to {objectedState}");
        }
    }
}