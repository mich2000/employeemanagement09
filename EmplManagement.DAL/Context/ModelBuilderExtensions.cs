﻿using EmplManagement.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace EmplManagement.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().HasData(
                new Employee() { Id = 1, Name = "Wim", Department = Department.HR, Email = "test@test.com" },
                new Employee() { Id = 2, Name = "Jack", Department = Department.IT, Email = "jack@test.com" },
                new Employee() { Id = 3, Name = "Paul", Department = Department.Payroll, Email = "paul@test.com" }
                );
        }
    }
}
