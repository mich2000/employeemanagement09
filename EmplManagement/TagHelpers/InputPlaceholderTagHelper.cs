﻿using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace EmplManagement.TagHelpers
{
    [HtmlTargetElement("input", Attributes = ForAttributeName)]
    public class InputPlaceholderTagHelper : InputTagHelper
    {
        private const string ForAttributeName = "asp-for";

        public ModelExpression AspFor { get; set; }

        public InputPlaceholderTagHelper(IHtmlGenerator generator) : base(generator)
        {
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.SetAttribute(new TagHelperAttribute("placeholder", AspFor.Name));
        }
    }
}
