﻿using EmplManagement.DAL.Models;
using Microsoft.AspNetCore.Http;

namespace EmplManagement.ViewModels
{
    public class EmployeeCreateViewModel : Employee
    {
        public IFormFile Photo { get; set; }
    }
}
