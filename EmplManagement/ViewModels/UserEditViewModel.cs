﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmplManagement.ViewModels
{
    public class UserEditViewModel
    {
        public UserEditViewModel()
        {
            Roles = new List<string>();
            Claims = new List<string>();
        }
        public string Id { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [RegularExpression(
            @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
            ErrorMessage = "The email is not in correct format.")]
        public string Email { get; set; }

        public string City { get; set; }

        public List<string> Roles { get; set; }

        public List<string> Claims { get; set; }
    }
}
