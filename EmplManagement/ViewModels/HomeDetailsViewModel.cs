﻿using EmplManagement.DAL.Models;

namespace EmplManagement.ViewModels
{
    public class HomeDetailsViewModel
    {
        public HomeDetailsViewModel(Employee employee, string pageTitle)
        {
            Employee = employee;
            PageTitle = pageTitle;
        }

        public Employee Employee { get; set; }
        public string PageTitle { get; set; }
    }
}
