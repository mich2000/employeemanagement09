﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmplManagement.DAL.Models;
using EmplManagement.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmplManagement.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdministrationController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AdministrationController(RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ListRoles()
        {
            return View(_roleManager.Roles);
        }

        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(RoleCreateViewModel model)
        {
            if(ModelState.IsValid)
            {
                IdentityRole role = new IdentityRole(model.RoleName);
                IdentityResult result = await _roleManager.CreateAsync(role).ConfigureAwait(false);
                if(result.Succeeded)
                {
                    return RedirectToAction("ListRoles", "Administration");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> EditRole(string Id)
        {
            var role = await _roleManager.FindByIdAsync(Id);
            if(role == null)
            {
                ViewBag.ErrorMessage = $"Role with Id {Id} cannot be found";
                return View("NotFound");
            }
            var model = new RoleEditViewModel()
            {
                Id = role.Id,
                RoleName = role.Name
            };
            var listUsers = await _userManager.Users.ToListAsync();
            foreach (var user in listUsers)
            {
                var isInRole = await _userManager.IsInRoleAsync(user, role.Name);
                if(isInRole)
                {
                    model.Users.Add(user.UserName);
                }
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditRole(RoleEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var role = await _roleManager.FindByIdAsync(model.Id);
                if(role == null)
                {
                    ViewBag.ErrorMessage = $"Role with Id {model.Id} cannot be found";
                    return View("NotFound");
                }
                role.Name = model.RoleName;
                var result = await _roleManager.UpdateAsync(role);
                if(result.Succeeded)
                {
                    return RedirectToAction("ListRoles", "Administration");
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty,error.Description);
                }
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> EditUserInRole(string roleId)
        {
            ViewBag.RoleId = roleId;
            var role = await _roleManager.FindByIdAsync(roleId);
            if (role == null)
            {
                ViewBag.ErrorMessage = $"Role with Id {roleId} cannot be found";
                return View("NotFound");
            }
            var listUsers = await _userManager.Users.ToListAsync();
            var model = new List<UserRoleViewModel>();
            foreach (var user in listUsers)
            {
                var userRoleVM = new UserRoleViewModel()
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                };
                var isInRole = await _userManager.IsInRoleAsync(user, role.Name);
                userRoleVM.IsSelected = isInRole;
                model.Add(userRoleVM);
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditUserInRole(List<UserRoleViewModel> model, string roleId)
        {
            if (ModelState.IsValid)
            {
                var role = await _roleManager.FindByIdAsync(roleId);
                if (role == null)
                {
                    ViewBag.ErrorMessage = $"Role with Id {roleId} cannot be found";
                    return View("NotFound");
                }
                for (int i = 0; i < model.Count(); i++)
                {
                    var user = await _userManager.FindByIdAsync(model[i].UserId);
                    var isInRole = await _userManager.IsInRoleAsync(user, role.Name);
                    IdentityResult result = null;
                    if (!isInRole && model[i].IsSelected)
                        result = await _userManager.AddToRoleAsync(user, role.Name);
                    else if (isInRole && !model[i].IsSelected)
                        result = await _userManager.RemoveFromRoleAsync(user, role.Name);
                    else
                        continue;
                    if (result.Succeeded)
                    {
                        if (i < (model.Count - 1))
                            continue;
                        else
                            return RedirectToAction("EditRole", "Administration", new { Id = roleId });
                    }
                }
            }
            return RedirectToAction("EditRole", "Administration", new { Id = roleId });
        }

        [HttpGet]
        public IActionResult ListUsers()
        {
            var roles = _userManager.Users.ToList();
            return View(roles);
        }

        [HttpGet]
        public async Task<IActionResult> EditUser(string Id)
        {
            var user = await _userManager.FindByIdAsync(Id);
            if (user == null)
            {
                ViewBag.ErrorMessage = $"Role with Id {Id} cannot be found";
                return View("NotFound");
            }
            var userClaims = await _userManager.GetClaimsAsync(user);
            var userRoles = await _userManager.GetRolesAsync(user);
            var model = new UserEditViewModel()
            {
                Id = user.Id,
                City = user.City,
                Email = user.Email,
                UserName = user.UserName,
                Claims = userClaims.Select(claim => claim.Value).ToList(),
                Roles = userRoles.ToList(),
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditUserAsync(UserEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(model.Id);
                if (user == null)
                {
                    ViewBag.ErrorMessage = $"Role with Id {model.Id} cannot be found";
                    return View("NotFound");
                }
                user.Email = model.Email;
                user.UserName = model.UserName;
                user.City = model.City;
                var result = await _userManager.UpdateAsync(user);
                if(result.Succeeded)
                {
                    return RedirectToAction("ListUsers", "Administration"); 
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty,error.Description);
                }
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUserAsync(string Id)
        {
            var user = await _userManager.FindByIdAsync(Id);
            if (user == null)
            {
                ViewBag.ErrorMessage = $"Role with Id {Id} cannot be found";
                return View("NotFound");
            }
            var result = await _userManager.DeleteAsync(user);
            if (result.Succeeded)
            {
                return RedirectToAction("ListUsers", "Administration");
            }
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
            return RedirectToAction("ListUsers", "Administration");
        }
    }
}