﻿using System;
using System.IO;
using EmplManagement.DAL.Classes;
using EmplManagement.DAL.Models;
using EmplManagement.DAL.Repositories;
using EmplManagement.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EmplManagement.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ILogger _logger;

        public HomeController(IEmployeeRepository employeeRepository, IWebHostEnvironment webHostEnvironment
            ,ILogger<HomeController> logger)
        {
            _employeeRepository = employeeRepository;
            _webHostEnvironment = webHostEnvironment;
            _logger = logger;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View(_employeeRepository.GetAll());
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var employee = _employeeRepository.GetById(id);
            if (employee == null)
            {
                throw ExceptionFactory.NullEx("employee");
            }

            if (employee.Id == 0)
            {
                throw ExceptionFactory.ArgsEx("id", "0", "employee.id");
            }

            if (employee != null && employee.Id != 0)
            {
                return View(new HomeDetailsViewModel(employee, "Employee Details"));
            }
            Response.StatusCode = 404;
            return View("EmployeeNotFound", id);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(EmployeeCreateViewModel model)
        {
            if (model == null)
                throw new ArgumentNullException("A empty form is not approved", "model");
            if (ModelState.IsValid)
            {
                if (model.Photo != null)
                {
                    model.PhotoPath = ProcessUploadFile(model);
                }
                else
                {
                    throw ExceptionFactory.NullEx("user's photo", "model.PhotoPath");
                }
                Employee newEmployee = _employeeRepository.Create(model);
                if (newEmployee != null)
                {
                    return RedirectToAction("Details", new { Id = newEmployee.Id });
                }
                else
                {
                    throw ExceptionFactory.NullEx("employee's id", "newEmployee.Id"); 
                }
            }
            return View();
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            Employee employee = _employeeRepository.GetById(id);
            if (employee == null)
            {
                throw ExceptionFactory.NullEx("employee");
            }

            if (employee.Id == 0)
            {
                throw ExceptionFactory.ArgsEx("Employee's id","0", "employee.Id");
            }

            EmployeeEditViewModel employeeEditVM = new EmployeeEditViewModel()
            {
                Id = employee.Id,
                Department = employee.Department,
                Email = employee.Email,
                Name = employee.Name,
                ExistingPhotoPath = employee.PhotoPath
            };

            if (employeeEditVM == null)
            {
                throw ExceptionFactory.NullEx("Edit form", "employeeEditVM");
            }

            return View(employeeEditVM);
        }

        [HttpPost]
        public IActionResult Update(EmployeeEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Photo != null)
                {
                    var photoExist = DeleteImage(model.ExistingPhotoPath);
                    if (!photoExist)
                    {
                        model.PhotoPath = ProcessUploadFile(model);
                    }
                }

                var employee = new Employee()
                {
                    Id = model.Id,
                    Department = model.Department,
                    Email = model.Email,
                    Name = model.Name,
                    PhotoPath = (model.PhotoPath == "") ? model.ExistingPhotoPath : model.PhotoPath
                };

                if(employee == null)
                {
                    throw ExceptionFactory.NullEx("employee");
                }

                if(employee.Id == 0)
                {
                    throw ExceptionFactory.ArgsEx("Employee's id", "0", "employee.Id");
                }

                Employee response = _employeeRepository.Update(employee);

                if (response == null)
                {
                    throw ExceptionFactory.NullEx("employee","response");
                }

                if (response.Id == 0)
                {
                    throw ExceptionFactory.ArgsEx("Employee's id", "0", "reponse.id");
                }
                return RedirectToAction("Index");
            }
            return View();
        }

        public IActionResult Delete(int id)
        {
            if (id == 0)
            {
                throw ExceptionFactory.ArgsEx("Employee's id", "0", "reponse.id");
            }
            Employee employee = _employeeRepository.GetById(id);
            if (employee == null)
            {
                throw ExceptionFactory.NullEx("employee");
            }
            return View(employee);
        }

        [HttpPost]
        public IActionResult DeleteShure(int id)
        {
            if(id == 0)
            {
                throw ExceptionFactory.ArgsEx("Employee's id", "0", "reponse.id");
            }
            Employee employee = _employeeRepository.GetById(id);
            if (employee == null)
            {
                throw ExceptionFactory.NullEx("employee");
            }
            if (employee != null && employee.Id != 0)
            {
                bool photoExist = DeleteImage(employee.PhotoPath);
                if (!photoExist)
                {
                    var response = _employeeRepository.Delete(id);
                    if (response != null && response.Id != 0)
                    {
                        return RedirectToAction("Index");
                    }
                }
                return View(employee);
            }
            return NotFound();
        }

        private bool DeleteImage(string photoPath)
        {
            if (photoPath != null)
            {
                var filePath = Path.Combine(_webHostEnvironment.WebRootPath, "images", photoPath);
                System.IO.File.Delete(filePath);
                return System.IO.File.Exists(filePath);
            }
            return false;
        }

        private string ProcessUploadFile(EmployeeCreateViewModel model)
        {
            string uniqueFileName = null;
            if (model.Photo != null)
            {
                var uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Photo.FileName;
                var filePath = Path.Combine(uploadsFolder, uniqueFileName);

                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    model.Photo.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }
    }
}