﻿using EmplManagement.DAL.Classes;
using EmplManagement.DAL.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EmplManagement.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        private readonly ILogger<ErrorController> _logger;

        public ErrorController(ILogger<ErrorController> logger)
        {
            _logger = logger;
        }

        [Route("Error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            IStatusCodeReExecuteFeature statusCodeResult = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            IUrl url = new UrlHolder(statusCode, statusCodeResult.OriginalPath, statusCodeResult.OriginalQueryString);
            _logger.LogWarning(url.ErrorLog());
            return View("NotFound",url);
        }

        [AllowAnonymous]
        [Route("Error")]
        public IActionResult ExceptionHandler()
        {
            IExceptionHandlerPathFeature exceptionHandlerPathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            WebFailure failure = new WebFailure(exceptionHandlerPathFeature.Error);
            _logger.LogError(failure.LogError());
            return View("Error",failure);
        }
    }
}