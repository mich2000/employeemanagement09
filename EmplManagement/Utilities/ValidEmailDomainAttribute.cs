﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmplManagement.Utilities
{
    public class ValidEmailDomainAttribute : ValidationAttribute
    {
        private string allowedDomain;

        public ValidEmailDomainAttribute(string allowedDomain)
        {
            this.allowedDomain = allowedDomain;
        }

        public override bool IsValid(object value)
        {
            String email = value.ToString();
            String[] emailArray = email.Split("@");
            String domain = emailArray.Last();
            return domain.ToUpper().Equals(allowedDomain.ToUpper());
        }
    }
}
